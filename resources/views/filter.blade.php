<?php
use App\Http\Controllers\CarManager;

$message = false;

if (isset($_GET['ganbajeba'])) {
    $cars = CarManager::getFilteredCars('ganbajeba', $_GET['ganbajeba']);

    $message = "Filtering By -- Ganbajebis Weli  --Value: " . $_GET['ganbajeba'] ;
} else if (isset($_GET['release'])) {
    $cars = CarManager::getFilteredCars('release_date', $_GET['release']);

    $message = "Filtering By -- Release Date  --Value: " . $_GET['release'] ;
} else {
    $cars = CarManager::getAllCars();
}
?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Filter Car List</title>
</head>
<body>
<div class="navigation">
    <p>Filter Cars Page</p>
    <a href="/">Go Back <--</a><br>
    <a href="/add-cars">Add Car</a><br>
</div>
<hr>
<?php if (isset($_GET['deleteSuccess'])): ?>
<p style="color: red; font-weight: bold;">Car Deleted Successfully!!</p>
<?php endif; ?>
<?php if ($message): ?>
<p style="color: green; font-weight: bold;"><?= $message ?></p>
<?php endif; ?>
<p>Filters:</p>
<form action="filterByRelease" method="post">
    @csrf
    <label for="releaseDate">Release Date (Gamoshvebis weli marto 2022,2020...):</label><br>
    <input type="text" id="releaseDate" name="releaseDate" placeholder="Gamosvlis Weli" required ><br><br>

    <input type="submit" value="Filter">
</form>
<hr>
<form action="filterByGanbajeba" method="post">
    @csrf
    <label for="ganbajeba">Ganbajebis Weli (Weli marto 2022,2020...):</label><br>
    <input type="text" id="ganbajeba" name="ganbajeba" placeholder="Ganbajebis Weli" required><br><br>

    <input type="submit" value="Filter">
</form>
<hr>
<?php foreach ($cars as $car): ?>
<hr>
<p>Car Name: <?= $car->name ?></p>
<p>Model: <?= $car->model ?></p>
<a href="/info?id=<?= $car->id ?>">Show Information</a>  ||
<a href="/edit?id=<?= $car->id ?>">Edit</a>  ||
<form action="deleteCar" method="post">
    @csrf
    <input type="text" id="id" name="id" placeholder="id" required value="<?= $car->id ?>" style="display: none;">
    <input type="submit" value="Delete">
</form>
<hr>
<?php endforeach; ?>
</body>
</html>
