<?php
use App\Http\Controllers\CarManager;

$car = CarManager::getCar($_GET['id'])[0];
?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Car - <?= $car->id ?></title>
</head>
<body>
<div class="navigation">
    <a href="/show-cars">Go Back!</a>
</div>
<p>Edit Car - With ID -  <?= $car->id ?></p>
<hr>

<?php if (isset($_GET['success'])): ?>
    <p style="color: green; font-weight: bold;">Car Updated Successfully!!</p>
<?php endif; ?>

<form action="editCar" method="post">
    @csrf
    <input type="text" id="id" name="id" placeholder="id" required value="<?= $car->id ?>" style="display: none;"><br><br>

    <label for="name">Car Name:</label>
    <input type="text" id="name" name="name" placeholder="Car Name" required value="<?= $car->name ?>"><br><br>

    <label for="model">Car Model:</label>
    <input type="text" id="model" name="model" placeholder="Car Model" required value="<?= $car->model ?>"><br><br>

    <label for="release_date">Release Date:</label>
    <input type="text" id="release_date" name="release_date" placeholder="Release Date" required value="<?= $car->release_date ?>"><br><br>

    <label for="engine_cap">Engine Capacity:</label>
    <input type="text" id="engine_cap" name="engine_cap" placeholder="Engine Capacity" required value="<?= $car->engine_cap ?>"><br><br>

    <label for="ganbajeba">Ganbajeba:</label>
    <input type="text" id="ganbajeba" name="ganbajeba" placeholder="Ganbajeba" required value="<?= $car->ganbajeba ?>"><br><br>

    <label for="price">Price:</label>
    <input type="text" id="price" name="price" placeholder="price" required value="<?= $car->price ?>"><br><br>
    <br>
    <input type="submit" value="Save">
</form>
</body>
</html>
