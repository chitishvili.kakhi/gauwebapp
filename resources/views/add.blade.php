<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Car</title>
</head>
    <body>
    <div class="navigation">
        <a href="/">Go Back!</a><br>
        <a href="/show-cars">View all Cars</a>
    </div>
    <p>Add Car</p>
    <hr>

        <form action="addCar" method="post">
            @csrf
            <label for="name">Car Name:</label>
            <input type="text" id="name" name="name" placeholder="Car Name" required><br><br>

            <label for="model">Car Model:</label>
            <input type="text" id="model" name="model" placeholder="Car Model" required><br><br>

            <label for="release_date">Release Date:</label>
            <input type="text" id="release_date" name="release_date" placeholder="Release Date" required><br><br>

            <label for="engine_cap">Engine Capacity:</label>
            <input type="text" id="engine_cap" name="engine_cap" placeholder="Engine Capacity" required><br><br>

            <label for="ganbajeba">Ganbajeba:</label>
            <input type="text" id="ganbajeba" name="ganbajeba" placeholder="Ganbajeba" required><br><br>

            <label for="price">Price:</label>
            <input type="text" id="price" name="price" placeholder="price" required><br><br>
            <br>
            <input type="submit" value="Add Car">
        </form>
    </body>
</html>
