<?php
use App\Http\Controllers\CarManager;

$cars = CarManager::getCar($_GET['id']);
?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Car</title>
</head>
<body>
<div class="navigation">
    <p>View Car Page</p>
    <a href="/show-cars">Go Back <--</a><br>
</div>
<hr>
<?php foreach ($cars as $car): ?>
    <hr>
    <p>Car Name: <?= $car->name ?></p>
    <p>Model: <?= $car->model ?></p>
    <p>Release Date: <?= $car->release_date ?></p>
    <p>Engine Capacity: <?= $car->engine_cap ?></p>
    <p>Ganbajeba: <?= $car->ganbajeba ?></p>
    <p>Price: <?= $car->price ?></p>
    <hr>
<?php endforeach; ?>
</body>
</html>
