<?php
use App\Http\Controllers\CarManager;

$cars = CarManager::getAllCars();
?>
<!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Car List</title>
</head>
<body>
<div class="navigation">
    <p>Manage Cars Page</p>
    <a href="/">Go Back <--</a><br>
    <a href="/add-cars">Add Car</a><br>
</div>
<hr>
<?php if (isset($_GET['deleteSuccess'])): ?>
<p style="color: red; font-weight: bold;">Car Deleted Successfully!!</p>
<?php endif; ?>
<p></p>
<?php foreach ($cars as $car): ?>
<hr>
<p>Car Name: <?= $car->name ?></p>
<p>Model: <?= $car->model ?></p>
<a href="/info?id=<?= $car->id ?>">Show Information</a>  ||
<a href="/edit?id=<?= $car->id ?>">Edit</a>  ||
<form action="deleteCar" method="post">
    @csrf
    <input type="text" id="id" name="id" placeholder="id" required value="<?= $car->id ?>" style="display: none;">
    <input type="submit" value="Delete">
</form>
<hr>
<?php endforeach; ?>
</body>
</html>
