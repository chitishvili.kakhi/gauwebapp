<?php
namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class CarManager
 *
 * @package App\Http\Controllers
 */
class CarManager extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * CarManager constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Add Car Method
     *
     * @return RedirectResponse
     */
    public function addCar(): RedirectResponse
    {
        $request = $this->request;

        DB::table('cars')->insert([
            'name' => $request->get('name'),
            'model' => $request->get('model'),
            'release_date' => $request->get('release_date'),
            'engine_cap' => $request->get('engine_cap'),
            'ganbajeba' => $request->get('ganbajeba'),
            'price' => $request->get('price'),
        ]);

        return redirect()->route('show-cars');
    }

    public function editCar()
    {
        $request = $this->request;

        DB::table('cars')->where('id', '=', $request->get('id'))->update([
            'name' => $request->get('name'),
            'model' => $request->get('model'),
            'release_date' => $request->get('release_date'),
            'engine_cap' => $request->get('engine_cap'),
            'ganbajeba' => $request->get('ganbajeba'),
            'price' => $request->get('price'),
        ]);

        return redirect()->route('edit', ['id' => $request->get('id'), 'success' => 1]);
    }

    /**
     * Filter By Release Method
     *
     * @return RedirectResponse
     */
    public function filterByRelease(): RedirectResponse
    {
        $releaseDate = $this->request->get('releaseDate');

        return redirect()->route('filter', ['release' => $releaseDate]);
    }

    /**
     * Filter By Ganbajeba Method
     *
     * @return RedirectResponse
     */
    public function filterByGanbajeba(): RedirectResponse
    {
        $ganbajebisWeli = $this->request->get('ganbajeba');

        return redirect()->route('filter', ['ganbajeba' => $ganbajebisWeli]);
    }


    /**
     * Delete Car Method
     *
     * @return RedirectResponse
     */
    public function deleteCar(): RedirectResponse
    {
        $id = $this->request->get('id');
        DB::table('cars')->where('id', '=', $id)->delete();

        return redirect()->route('show-cars', ['deleteSuccess' => 1]);
    }

    /**
     * Get All Cars
     *
     * @return Collection
     */
    public static function getAllCars(): Collection
    {
        return DB::table('cars')->get();
    }

    /**
     * Get All Cars
     *
     * @param $id
     *
     * @return Collection
     */
    public static function getCar($id)
    {
        return DB::table('cars')->where('id', '=', $id)->get();
    }

    /**
     * Get Filtered list
     *
     * @param $field
     * @param $value
     *
     * @return Collection
     */
    public static function getFilteredCars($field, $value): Collection
    {
        return DB::table('cars')->where($field, '=', $value)->get();
    }
}
