<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarManager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('add-cars', function () {
    return view('add');
})->name('add-cars');

Route::get('show-cars', function () {
    return view('show');
})->name('show-cars');

Route::get('info', function () {
    return view('info');
})->name('info');

Route::get('edit', function () {
    return view('edit');
})->name('edit');

Route::get('filter', function () {
    return view('filter');
})->name('filter');


Route::POST('addCar',[CarManager::class, 'addCar']);
Route::POST('editCar',[CarManager::class, 'editCar']);
Route::POST('deleteCar',[CarManager::class, 'deleteCar']);
Route::POST('filterByRelease',[CarManager::class, 'filterByRelease']);
Route::POST('filterByGanbajeba',[CarManager::class, 'filterByGanbajeba']);
